#include <jni.h>
#include <android/log.h>
#include "storePCM.h"

#ifdef __cplusplus
extern "C"
{
#endif

JavaVM* javaVM = NULL;
jobject activity;

short EngineListener(tListener * pData)
{
    JNIEnv *env;
    javaVM->AttachCurrentThread(&env, NULL);
    jclass clazz = env->FindClass("com/example/hellojni/HelloJni");
    jmethodID callbackMethodId = env->GetMethodID(clazz, "engineCallback", "(II)V");
    env->CallVoidMethod(activity, callbackMethodId, pData->iMessage, pData->iSum);
    /*switch(pData->iMessage)
    {
        case  1:
            printf("short time energy %ld\n", pData->iSum);
            break;
        default:
            printf("Unsupported message %d\n",pData->iMessage);
    }*/
    return RETURN_OK;
}

JNIEXPORT jint JNICALL Java_com_example_hellojni_HelloJni_pcmTest( JNIEnv* env,
                                                  jobject thiz ) {
    env->GetJavaVM(&javaVM);
    activity = thiz;

    short iRet;

    __android_log_print(ANDROID_LOG_INFO, "hello-jni.cpp", "hello\n");

    tStorerSettings stStorerSettings;
    stStorerSettings.EnergyWindowSize = 800;    // at 8kHz rate each 100ms
    //stStorerSettings.EnergyWindowSize = 0;    // no callback with energy

    storePCM storer(stStorerSettings);

    iRet = storer.RegisterListener(&EngineListener,NULL);
    if (iRet != RETURN_OK)
    {
        __android_log_print(ANDROID_LOG_INFO, "hello-jni.cpp", "chyba registrace listeneru\n");
        return -1;
    }

    iRet = storer.OpenFile("/sdcard/test.pcm");
    if (iRet != RETURN_OK)
    {
        __android_log_print(ANDROID_LOG_INFO, "hello-jni.cpp", "chyba otevreni souboru\n");
        return -2;
    }

    short samples = 8000*2;
    short * buffer;
    buffer = new short[samples];
    for (int i = 0; i< samples;i++)
        buffer[i] = i%5000;

    iRet = storer.AddData(buffer,samples);
    if (iRet != RETURN_OK)
    {
        __android_log_print(ANDROID_LOG_INFO, "hello-jni.cpp", "chyba ulozeni dat\n");
        return -1;
    }

    iRet = storer.CloseFile();
    if (iRet != RETURN_OK)
        return -1;
}

#ifdef __cplusplus
}
#endif