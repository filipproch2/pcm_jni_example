/** @file
* example of stroing PCM data into file
*
* @author Lubos Smidl
*/

#ifndef __storePCM
#define __storePCM

#include <stdio.h>

#define RETURN_OK               0
#define RETURN_FAIL             1
#define RETRUN_ERROR_OPEN_FILE  2

struct tListener
{
  short iMessage;          
  unsigned long iSum;     
};

struct tStorerSettings
{
    short EnergyWindowSize;
};


class storePCM
{
protected:
  FILE * f;
  short (*m_pListener)(tListener *);
  short CallListener(tListener * petListener);
  short m_sEnergyWindowSize;

public:	
  storePCM(tStorerSettings stStorerSettings);
	~storePCM(void);

  short RegisterListener(short(*pListener)(tListener * petListener), void * pUserData);

	short OpenFile(const char *pchName); 
	short CloseFile();
	short AddData(short * pSamples, short nSamples);
};


#endif //__storePCM