/** @file
* example of storing PCM data into file
*
* @author Lubos Smidl
*/

#include "storePCM.h"
#include <stdlib.h>
#include <android/log.h>

storePCM::storePCM(tStorerSettings stStorerSettings)
{
  f = NULL;
  m_sEnergyWindowSize = stStorerSettings.EnergyWindowSize;
}

storePCM::~storePCM(void)
{
  if ( f!= NULL)
    fclose(f);
}

short storePCM::OpenFile(const char *pchName)
{
  f = fopen( pchName, "wb" );
  if (f == NULL )
  {
    __android_log_print(ANDROID_LOG_INFO, "storePCM.cpp", "cannot open file %s\n", pchName);
    return RETRUN_ERROR_OPEN_FILE; 
  }

  return RETURN_OK;
}

short storePCM::CloseFile()
{
  fclose(f);
  f = NULL;
  return RETURN_OK;
}

short storePCM::AddData(short * pSamples, short nSamples)
{
  if (f == NULL)
    return RETURN_FAIL;

  if (m_sEnergyWindowSize > 0)
  {
    tListener ttListener;
    ttListener.iMessage = 1;
    long sum = 0;
    for(int i=0;i<nSamples;i++)
    {
      sum+=abs(pSamples[i]);
      if ((i+1)%m_sEnergyWindowSize == 0)
      {
        ttListener.iSum = sum;
        CallListener(&ttListener);
        sum = 0;
      }
    }
  }

  short num = fwrite(pSamples, sizeof(short), nSamples, f);
  if (num != nSamples)
    return RETURN_FAIL;
  
  return RETURN_OK;
}

short storePCM::RegisterListener(short(*pListener)(tListener * petListener), void * pUserData)
{
  //m_pUserData = pUserData;
  m_pListener = pListener;
  return RETURN_OK;
}

short storePCM::CallListener(tListener * petListener)
{
  short iRet;
  if(m_pListener)
	{
		//petListener->pUserData = m_pUserData; 
		
		iRet = (* m_pListener)(petListener);

    // test iRet
	}
	
  return RETURN_OK;
}